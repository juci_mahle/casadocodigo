package br.com.casadocodigo.loja.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProdutosController {

	@RequestMapping("produtos/form")
	public String form() {
		return "produto/form";
	}
	
	@RequestMapping("produtos")
	public String gravar(String titulo, String descricao, int paginas) {
		System.out.println(titulo);
		System.out.println(descricao);
		System.out.println(paginas);
		
		return "ok";
	}
}
